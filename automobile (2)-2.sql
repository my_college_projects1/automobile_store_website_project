-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2021 at 03:33 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `automobile`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`email`, `password`) VALUES
('admin678@gmail.com', '567890');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `name` varchar(200) NOT NULL,
  `lname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `date` varchar(200) NOT NULL,
  `country` varchar(200) NOT NULL,
  `message` varchar(200) NOT NULL,
  `reasonmessage` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`name`, `lname`, `email`, `date`, `country`, `message`, `reasonmessage`) VALUES
('ailjzcil', ' akl', 'mandeepsingh110696@gmail.com', '1334443', '>Mexico', 'jkNckl', 'jkZN l');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `cust_id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`cust_id`, `email`, `password`) VALUES
(1, 'mandeep888@gmail.com', '345678'),
(2, 'mandeep888@gmail.com', '345678'),
(3, 'mandeep19@gmail.com', '456756'),
(4, 'test@gmail.com', '345678');

-- --------------------------------------------------------

--
-- Table structure for table `exchange_car`
--

CREATE TABLE `exchange_car` (
  `ex_id` int(100) NOT NULL,
  `brand` varchar(200) NOT NULL,
  `model` varchar(200) NOT NULL,
  `color` varchar(200) NOT NULL,
  `car_desc` varchar(200) NOT NULL,
  `current_owner` varchar(200) NOT NULL,
  `price` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `exchange_car`
--

INSERT INTO `exchange_car` (`ex_id`, `brand`, `model`, `color`, `car_desc`, `current_owner`, `price`) VALUES
(3, 'jlnckla', 'kladz', 'klaz', 'klam', 'klajk', '4445'),
(4, 'ajcn', 'klac', 'jlanc', 'njad ', 'al', '100'),
(5, 'ajcn', 'klac', 'jlanc', 'zn', 'klk', '3444'),
(6, 'ajcn', 'klac', 'jlanc', 'am', 'al', '34'),
(7, 'ajcn', 'klac', 'jlanc', 'klmackl', 'al', '34533'),
(8, 'ajcn', 'klac', 'd', ' m', 'k', '455'),
(9, 'ajcn', 'klac', 'jlanc', 'za', 'klk', '555'),
(10, 'ajcn', 'klac', 'jlanc', 'z', 'al', '333'),
(11, 'ajcn', 'klac', 'jlanc', 'c', 'c', '100'),
(12, 'ajcn', 'klac', 'jlanc', 'df', 'al', '345'),
(13, 'ajcn', 'klac', 'jlanc', 'zvcc', 'klk', '5354'),
(14, 'ajcn', 'klac', 'jlanc', 'm', 'klk', '335'),
(15, 'ajcn', 'klac', 'jlanc', 'nk', 'al', '677'),
(16, 'ajcn', 'klac', 'black', 'klackl', 'ac', '134');

-- --------------------------------------------------------

--
-- Table structure for table `finance_car`
--

CREATE TABLE `finance_car` (
  `fin_id` int(200) NOT NULL,
  `loan_term` varchar(200) NOT NULL,
  `installment_interval` varchar(200) NOT NULL,
  `InvtId` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `finance_car`
--

INSERT INTO `finance_car` (`fin_id`, `loan_term`, `installment_interval`, `InvtId`) VALUES
(5, '5', 'BiWeekly', '3'),
(6, '5', 'BiWeekly', '3'),
(7, '5', 'BiWeekly', '3'),
(8, '5', 'BiWeekly', '3'),
(9, '5', 'BiWeekly', '3'),
(10, '5', 'BiWeekly', '3'),
(11, '5', 'Monthly', '3'),
(12, '10', 'Monthly', '3'),
(13, '5', 'BiWeekly', '3'),
(14, '5', 'BiWeekly', '17'),
(15, '5', 'BiWeekly', '17'),
(16, '5', 'BiWeekly', '10'),
(17, '5', 'BiWeekly', '10'),
(18, '5', 'BiWeekly', '15'),
(19, '5', 'BiWeekly', '11'),
(20, '5', 'BiWeekly', '14');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `InvtId` int(11) NOT NULL,
  `brand` varchar(200) NOT NULL,
  `model` varchar(200) NOT NULL,
  `price` varchar(200) NOT NULL,
  `color` varchar(200) NOT NULL,
  `year` varchar(200) NOT NULL,
  `type` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `Image` varchar(200) NOT NULL,
  `isBooked` int(200) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`InvtId`, `brand`, `model`, `price`, `color`, `year`, `type`, `description`, `Image`, `isBooked`) VALUES
(11, 'BMW', 'X6', '700$', 'Black', '2015', 'Suv', 'great look exceptional speed', '../Images/b8.jpg', 0),
(13, 'BMW', 'M550I', '600$', 'White', '2015', 'Economy', 'great look exceptional speed', '../Images/b13.jpg', 0),
(14, 'BMW', 'M550I', '600$', 'Black', '2015', 'Economy', 'great look exceptional speed', '../Images/b14.jpg', 1),
(15, 'Hyundai', 'Accent', '600$', 'Black', '2015', 'Economy', 'great look exceptional speed', '../Images/h1.jpg', 1),
(16, 'Hyundai', 'Accent', '600$', 'White', '2015', 'Economy', 'great look exceptional speed', '../Images/h2.jpg', 1),
(18, 'Hyundai', 'Santa', '600$', 'Black', '2015', 'Suv', 'great look exceptional speed', '../Images/h4.jpg', 1),
(19, 'Hyundai', 'Santa', '600$', 'White', '2015', 'Suv', 'great look exceptional speed', '../Images/hh4.jpg', 1),
(20, 'Lamborghini', 'HURACAN', '600$', 'Black', '2015', 'Luxury', 'great look exceptional speed', '../Images/l4.jpg', 1),
(21, 'Lamborghini', 'Huracan', '600$', 'White', '2015', 'Luxury', 'great look exceptional speed', '../Images/l5.jpg', 1),
(22, 'Lamborghini', 'AVENTADOR', '600$', 'Black', '2015', 'Luxury', 'great look exceptional speed', '../Images/l6.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `email` varchar(200) NOT NULL,
  `fname` varchar(200) NOT NULL,
  `lname` varchar(200) NOT NULL,
  `pass` int(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `phno` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`email`, `fname`, `lname`, `pass`, `address`, `phno`) VALUES
('mandeellllp@gmail.com', 'mandeep', 'jnac', 456756, '1099 rue champlan', 2147483647),
('mandeep134@gmail.com', 'mandeep', 'singh', 456756, '103 rue champlan', 2147483647),
('mandeep13@gmail.com', 'mandeep', 'singh', 456756, '1039 rue champlan', 2147483647),
('mandeep19@gmail.com', 'mandeep', 'jnac', 456756, '1099 rue champlan', 2147483647),
('mandeep4@gmail.com', 'mandeep', 'jnac', 456756, '1099 rue champlan', 2147483647),
('mandeep888@gmail.com', 'mandeep', 'singh', 345678, '1039 rue champlan woodland', 2147483647),
('mandeep88@gmail.com', 'mandeep', 'jnac', 345678, '1099 rue champlan', 2147483647),
('mandeep@gmail.com', 'mandeep', 'singh', 456756, '1039 rue champlan', 2147483647),
('mandeepilj@gmail.com', 'mandeep', 'jnac', 456756, '103 rue champlan', 2147483647),
('mandeepsingh@gmail.com', 'mandeep', 'singh', 456756, '1039 rue champlan', 2147483647),
('mandeetp@gmail.com', 'mandeep', 'jnac', 456756, '103 rue champlan', 2147483647),
('test@gmail.com', 'test', 'test', 345678, '1039 rue champlan woodland', 2147483647);

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `res_id` int(100) NOT NULL,
  `invt_id` int(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `fname` varchar(200) NOT NULL,
  `lname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `Phoneno` varchar(200) NOT NULL,
  `sdate` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`res_id`, `invt_id`, `address`, `fname`, `lname`, `email`, `Phoneno`, `sdate`) VALUES
(2, 10, '10394 rue cjha', 'mandeep', 'singh', 'mandeep4@gmail.com', '6660988977', '2021-05-26'),
(43, 2, '10394 rue cjha', 'mandeep', 'jnac', 'jnl@gmial.com', '6660988977', ''),
(44, 3, '10394 rue cjha', 'mandeep', 'jnac', 'jnl@gmial.com', '6660988977', ''),
(45, 3, 'dfdf', 'mandeep', 'singh', 'jnl@gmial.com', '6660988977', ''),
(46, 3, 'dfdf', 'mandeep', 'singh', 'jnl@gmial.com', '6660988977', ''),
(47, 3, 'dfdf', 'mandeep', 'singh', 'jnl@gmial.com', '6660988977', ''),
(48, 3, 'dfdf', 'mandeep', 'singh', 'jnl@gmial.com', '6660988977', ''),
(49, 3, 'dfdf', 'mandeep', 'singh', 'jnl@gmial.com', '6660988977', ''),
(50, 3, 'dfdf', 'mandeep', 'singh', 'jnl@gmial.com', '6660988977', ''),
(51, 3, 'dfdf', 'mandeep', 'singh', 'jnl@gmial.com', '6660988977', ''),
(52, 3, 'dfdf', 'mandeep', 'singh', 'jnl@gmial.com', '6660988977', ''),
(53, 3, 'dfdf', 'mandeep', 'singh', 'jnl@gmial.com', '6660988977', ''),
(54, 3, 'dfdf', 'mandeep', 'singh', 'jnl@gmial.com', '6660988977', ''),
(55, 3, 'dfdf', 'mandeep', 'singh', 'jnl@gmial.com', '6660988977', ''),
(56, 3, 'dfdf', 'mandeep', 'singh', 'jnl@gmial.com', '6660988977', ''),
(57, 3, 'dfdf', 'mandeep', 'singh', 'jnl@gmial.com', '6660988977', ''),
(58, 3, 'dfdf', 'mandeep', 'singh', 'jnl@gmial.com', '6660988977', ''),
(59, 3, 'dfdf', 'mandeep', 'singh', 'jnl@gmial.com', '6660988977', ''),
(60, 3, 'dfdf', 'mandeep', 'singh', 'jnl@gmial.com', '6660988977', ''),
(61, 2, '10394 rue cjha', 'mandeep', 'jnac', 'jnl@gmial.com', '6660988977', ''),
(62, 2, '10394 rue cjha', 'mandeep', 'jnac', 'jnl@gmial.com', '6660988977', ''),
(63, 3, 'dfdf', 'mandeep', 'jnac', 'jnl@gmial.com', '6660988977', ''),
(64, 3, 'dfdf', 'mandeep', 'jnac', 'jnl@gmial.com', '6660988977', ''),
(65, 3, '10394 rue cjha', 'mandeep', 'jnac', 'jnl@gmial.com', '6660988977', ''),
(66, 3, '10394 rue cjha', 'mandeep', 'jnac', 'jnl@gmial.com', '6660988977', ''),
(67, 3, '10394 rue cjha', 'mandeep', 'jnac', 'jnl@gmial.com', '6660988977', ''),
(68, 3, 'dfdf', 'mandeep', 'jnac', 'jnl@gmial.com', '6660988977', ''),
(69, 3, 'dfdf', 'mandeep', 'jnac', 'jnl@gmial.com', '6660988977', ''),
(70, 3, 'dfdf', 'mandeep', 'jnac', 'jnl@gmial.com', '6660988977', ''),
(71, 3, 'dfdf', 'mandeep', 'jnac', 'jnl@gmial.com', '6660988977', ''),
(87, 10, 'dfdf', 'mandeep', 'jnac', 'mandeep19@gmail.com', '4389282468', '2021-05-26'),
(88, 10, 'dfdf', 'mandeep', 'jnac', 'jnl@gmial.com', '6660988977', '2021-05-28'),
(89, 10, 'dfdf', 'mandeep', 'jnac', 'jnl@gmial.com', '6660988977', '2021-05-28'),
(95, 13, '10394 rue cjha', 'najknc', 'jnac', 'mandeep19@gmail.com', '4389282466', '2021-07-09'),
(102, 13, '10394 rue cjha', 'mandeep', 'jnac', 'test@gmail.com', '4389282466', '2021-07-08'),
(103, 11, '10394 rue cjha', 'test', 'jnac', 'test@gmail.com', '4389282468', '2021-07-08');

-- --------------------------------------------------------

--
-- Table structure for table `test_drive`
--

CREATE TABLE `test_drive` (
  `test_id` int(200) NOT NULL,
  `cemail` varchar(200) NOT NULL,
  `testdate` varchar(200) NOT NULL,
  `InvtId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `test_drive`
--

INSERT INTO `test_drive` (`test_id`, `cemail`, `testdate`, `InvtId`) VALUES
(45, 'test@gmail.com', '2021-06-17', 13);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`cust_id`);

--
-- Indexes for table `exchange_car`
--
ALTER TABLE `exchange_car`
  ADD PRIMARY KEY (`ex_id`);

--
-- Indexes for table `finance_car`
--
ALTER TABLE `finance_car`
  ADD PRIMARY KEY (`fin_id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`InvtId`);

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`res_id`);

--
-- Indexes for table `test_drive`
--
ALTER TABLE `test_drive`
  ADD PRIMARY KEY (`test_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `cust_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `exchange_car`
--
ALTER TABLE `exchange_car`
  MODIFY `ex_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `finance_car`
--
ALTER TABLE `finance_car`
  MODIFY `fin_id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `InvtId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `res_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `test_drive`
--
ALTER TABLE `test_drive`
  MODIFY `test_id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
